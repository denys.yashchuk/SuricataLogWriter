# SuricataLoWriter
## The application for writing alert logs to the (remote) database.
### The installation script is for Ubuntu 16.04. Installs Suricata 3.0 and 
### SuricataLogWriter, which writes alerts into MySQL database, and adds 
### services for running them in background

##Instructions
###Preinstallation (configuration database)
If you do not have MySQL installed - go ahead and do so:
```
 sudo apt-get update && sudo apt-get upgrade
 sudo apt-get install mysql-server mysql-client
```
For MySQL make sure you create a db, a user and a table:
creating db
```
mysql>create database DB_NAME;
```
creating user and granting privileges
```
mysql> create user 'USERNAME'@'HOST_IP' IDENTIFIED BY 'PASSWORD';
```
(HOST_IP is IP address of host, where suricata will be running)
```
mysql> grant all privileges on DB_NAME.* to 'USERNAME'@'HOST_IP' with grant option;
mysql> flush privileges;
```
changing db
```
mysql> use DB_NAME;
```
for creating tables use db.sql script from archive or next commands:
```
mysql> CREATE TABLE ALERT (ALERT_ID BIGINT, TIMESTAMP DATE, FLOW_ID BIGINT, 
	IN_IFACE VARCHAR(20), 
	EVENT_TYPE VARCHAR(20), 
	SRC_IP VARCHAR(39), 
	SRC_PORT BIGINT, 
	DEST_IP VARCHAR(39), 
	DEST_PORT BIGINT, 
	PROTO VARCHAR(20), 
	ICMP_TYPE BIGINT, 
	ICMP_CODE BIGINT, 
	ALERT_INFO_ID BIGINT
   );
mysql> CREATE TABLE ALERT_INFO 
   (	ALERT_INFO_ID BIGINT, 
	ACTION VARCHAR(20), 
	GID BIGINT, 
	SIGNATURE_ID BIGINT, 
	REV BIGINT, 
	SIGNATURE VARCHAR(50), 
	CATEGORY VARCHAR(50), 
	SEVERTY BIGINT
   );
mysql> COMMIT;
```
###Suricata and SuricataLogWriter installation
installation of GIT
```
sudo apt-get update
sudo apt-get install git
```
downloading SuricataLogWriter.tar.gz
```
git clone git@gitlab.com:denys.yashchuk/SuricataLogWriter.git
```
installing
```
cd SuricataLogWriter/
tar -xvf SuricataLogWriter.tar.gz
cd SuricataLogWriter/
sudo chmod +x install.sh
sudo ./install.sh
```
while installation you will be asked for the IP address of the host, where MySQL
server is running, the name of the database (DB_NAME), the USERNAME and the PASSWORD
Installation is finished. Suricata and SuricataLogWriter will start automatically.